= Shutterizer

I have 3 windows I want to open and close using chain actuators (D+H CDC 200/600)

Unfortunatly the protocol used to control the actuators is not documented, so I cannot
command the motors to a specific position, only control direction.

The windows are hinged at the bottom, so a robust way to get position feedback from
the window is to use an accelerometer.

