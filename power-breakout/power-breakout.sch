EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5B9CCDC8
P 1800 1900
F 0 "J1" H 1880 1892 50  0000 L CNN
F 1 "Out-A" H 1700 1550 50  0000 L CNN
F 2 "local:Molex_KK-6410-04_04x2.54mm_Straight" H 1800 1900 50  0001 C CNN
F 3 "~" H 1800 1900 50  0001 C CNN
	1    1800 1900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5B9CCE17
P 3200 1900
F 0 "J2" H 3280 1892 50  0000 L CNN
F 1 "In" H 3200 1550 50  0000 L CNN
F 2 "local:Molex_KK-6410-04_04x2.54mm_Straight" H 3200 1900 50  0001 C CNN
F 3 "~" H 3200 1900 50  0001 C CNN
	1    3200 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5B9CCE63
P 4200 1900
F 0 "J3" H 4280 1892 50  0000 L CNN
F 1 "Out-B" H 4100 1550 50  0000 L CNN
F 2 "local:Molex_KK-6410-04_04x2.54mm_Straight" H 4200 1900 50  0001 C CNN
F 3 "~" H 4200 1900 50  0001 C CNN
	1    4200 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1900 3000 1900
Wire Wire Line
	3000 1900 4000 1900
Connection ~ 3000 1900
Wire Wire Line
	2000 2000 3000 2000
Wire Wire Line
	3000 2000 4000 2000
Connection ~ 3000 2000
$Comp
L Device:Polyfuse_Small F1
U 1 1 5B9CD0E6
P 2500 2100
F 0 "F1" V 2300 2100 50  0000 C CNN
F 1 "750 mA" V 2400 2100 50  0000 C CNN
F 2 "Capacitors_THT.pretty:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 2550 1900 50  0001 L CNN
F 3 "~" H 2500 2100 50  0001 C CNN
	1    2500 2100
	0    -1   -1   0   
$EndComp
$Comp
L Device:Polyfuse_Small F2
U 1 1 5B9CD141
P 3650 2100
F 0 "F2" V 3850 2100 50  0000 C CNN
F 1 "750 mA" V 3750 2100 50  0000 C CNN
F 2 "Capacitors_THT.pretty:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 3700 1900 50  0001 L CNN
F 3 "~" H 3650 2100 50  0001 C CNN
	1    3650 2100
	0    1    1    0   
$EndComp
Connection ~ 3000 1800
Wire Wire Line
	3550 2100 3000 2100
Wire Wire Line
	3000 2100 2600 2100
Connection ~ 3000 2100
Wire Wire Line
	2400 2100 2000 2100
Wire Wire Line
	4000 2100 3750 2100
Wire Wire Line
	3000 1800 4000 1800
Wire Wire Line
	2000 1800 3000 1800
$EndSCHEMATC
