#include <avr/wdt.h> 
#include <stdio.h>
#include <util/delay.h>

#include "board.h"

#include "leds.h"
#include "uart.h"
#include "motor.h"
#include "command.h"
#include "doorsensor.h"
#include "supplysense.h"

int main(void) {
  wdt_enable(WDTO_4S);
  ledInit();
  ledBooting();
  uartInit();
  P("Booting %d\r\n", SHUTTER_ADDRESS);

  doorSensorInit();
  commandInit();
  motorInit();
  supplySenseInit();
  ledBooted();
  
  uint16_t loop = 0;
  while (1) {
    wdt_reset();
    
    if (!loop++) {
    }

    CommandState cmd = commandReady();
    if (cmd) {
      // Handle command.
      if (cmd == CS_OPEN) {
        L("OK");
        motorOpen();
        
      } else if (cmd == CS_CLOSE) {
        L("OK");
        motorClose();
         
      } else if (cmd == CS_STOP) {
        L("OK");
        motorStop();
        
      } else if (cmd == CS_REPORT) {
	int16_t openness = doorSensorRead();
	int16_t tempDC = doorSensorTemperature();
	uint16_t supplymV = supplySenseRead();
	
	P("open=%d temp=%d supply=%d\r\n", openness, tempDC, supplymV);      
        
      } else if (cmd == CS_GOTO) {
        P("Goto %d\r\n", commandArgument());
      }
    }
    
    
  }
}


