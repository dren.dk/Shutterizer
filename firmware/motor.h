#pragma once

void motorInit(void);
void motorOpen(void);
void motorClose(void);
void motorStop(void);
