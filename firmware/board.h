#pragma once

#include "avr8gpio.h"

/*
| Define         | AVR | Pin | Description |
|----------------|-----|-----|-------------|
|                | PD3 | 1   | Slave TX, Master RX |
|                | PD2 | 2   | Slave RX, Master TX |
|                | PD1 | 5   | SDA         |
|                | PD0 | 6   | SCL         |
|                | PD4 | 7   |             |
|                | PC6 | 8   |             |
|                | PD7 | 9   |             |
|                | PE6 | 10  |             |
|                | PB4 | 11  |             |
|                | PB5 | 12  |             |
|                | PB6 | 13  |             |
|                | PB2 | 14  |             |
|                | PB3 | 15  |             |
|                | PB1 | 16  |             |
|                | PF7 | 17  |             |
| SUPPLY_SENSE   | PF6 | 18  | Analog input from 1/11 voltage divider |
| MOTOR_OPEN     | PF5 | 19  | H-bridge open |
| MOTOR_CLOSE    | PF4 | 20  | H-bridge close |
*/

#define TWI_HARDWARE
#define TWI_SDA         GPD1
#define TWI_SCL         GPD0

#define LED_YELLOW     GPB0
#define LED_GREEN      GPD5

#define MOTOR_OPEN     GPF5
#define MOTOR_CLOSE    GPF4

#define SUPPLY_SENSE PF6
#define SUPPLY_SENSE_ADC 6


#if SHUTTER_ADDRESS==1

// Measure:
#define VCC 3280

// Fusk until reading correct voltage, neutral = 1000
#define SUPPLY_TRIM 1020

#elif SHUTTER_ADDRESS==2

// Measure:
#define VCC 3280

// Fusk until reading correct voltage, neutral = 1000
#define SUPPLY_TRIM 1020

#elif SHUTTER_ADDRESS==3

// Measure:
#define VCC 3280

// Fusk until reading correct voltage, neutral = 1000
#define SUPPLY_TRIM 1020

#else

#error "SHUTTER_ADDRESS not defined"

#endif

