#include "motor.h"
#include "board.h"

void motorInit(void) {
  GPOUTPUT(MOTOR_OPEN);
  GPOUTPUT(MOTOR_CLOSE);
  motorStop();
}

void motorOpen(void) {
  GPSET(MOTOR_OPEN);
  GPCLEAR(MOTOR_CLOSE);
}

void motorClose(void) {
  GPSET(MOTOR_CLOSE);
  GPCLEAR(MOTOR_OPEN);
}

void motorStop(void) {
  GPCLEAR(MOTOR_OPEN);
  GPCLEAR(MOTOR_CLOSE);
}
