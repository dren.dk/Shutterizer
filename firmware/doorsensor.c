#include <avr/io.h>
#include <util/twi.h>
#include <util/delay.h>
#include <math.h>

#include "doorsensor.h"
#include "uart.h"
#include "mpu6050.h"

void doorSensorInit(void) {
  MPU6050_Init();
}

int16_t doorSensorReadRaw(void) {
  int16_t accelRaw[3];
  mpu6050_readAccel(accelRaw);  
  return round(atan2(-accelRaw[2], accelRaw[1])*(1<<15)/M_PI_2);
}

int16_t doorSensorRead(void) {
  static int16_t avg = 0;

  int16_t d = doorSensorReadRaw()-avg;
  
  d = d >> 2;
  
  avg += d;
  
  return avg >> 5;
}


int16_t doorSensorTemperature(void) {
  return mpu6050_readTempDC();  
}
