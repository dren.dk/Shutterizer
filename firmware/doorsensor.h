#pragma once
#include <inttypes.h>

void doorSensorInit(void);
int16_t doorSensorRead(void);
int16_t doorSensorTemperature(void);
