#include "command.h"
#include "uart.h"

/*

!no  Open
!nc  Close
!ns  Stop

!nr  Read state (position, motor state)
!ngnnn Set position to nnn

*/

CommandState state;
uint16_t arg;

uint16_t commandArgument(void) {
  return arg;
}

void commandInit(void) {
  state = CS_IDLE;
  arg = 0;
}

CommandState commandReady(void) {
  int ch = getc(stdin);
  if (ch < 0 || ch > 0xff) {
    return CS_IDLE;
  }
  
  if (state == CS_IDLE) {
    if (ch == '!') {
      state = CS_ATTNTION;
    }
    
  } else if (state == CS_ATTNTION) {
    if (ch == '0'+SHUTTER_ADDRESS) {
      state = CS_SELECTED;
    } else {
      state = CS_IDLE;
    }    
    
  } else if (state == CS_SELECTED) {
    
    if (ch == 'o') {
      state = CS_OPEN;
    } else if (ch == 'c') {
      state = CS_CLOSE;
    } else if (ch == 's') {
      state = CS_STOP;
    } else if (ch == 'r') {
      state = CS_REPORT;      
    } else if (ch == 'g') {
      state = CS_GOTO;
      arg = 0;
    }
    
  } else if (state == CS_GOTO) {
    if (ch == '\r') {
      CommandState result = state;
      state = CS_IDLE;
      return result;
      
    } else if (ch >= '0' && ch <= '9') {
      arg *= 10;
      arg += ch - '0';
      
    } else {
      P("Bad char: '%c'\r\n", ch); 
      state = CS_IDLE;
    }    
  } else if (state == CS_OPEN || state == CS_CLOSE || state == CS_STOP || state == CS_REPORT) {
    if (ch == '\r') {
      CommandState result = state;
      state = CS_IDLE;
      return result;
    } else {
      P("Bad char: '%c'\r\n", ch); 
      state = CS_IDLE;
    }
  }
  
  return CS_IDLE;
}
