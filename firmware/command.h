#pragma once

#include <inttypes.h>

void commandInit(void);

typedef enum {
  CS_IDLE,
  CS_ATTNTION,
  CS_SELECTED,
  CS_OPEN,
  CS_CLOSE,
  CS_STOP,
  CS_REPORT,
  CS_GOTO,
} CommandState;

CommandState commandReady(void);
uint16_t commandArgument(void);
