#include "supplysense.h"
#include "adchelper.h"
#include "board.h"

void supplySenseInit(void) {
  initADC();
  GPINPUT(SUPPLY_SENSE);
}


// Resistor in voltage divider from ADC to ground
#define R_GND 1

// Resistor in voltage divider from ADC to supply voltage
#define R_SUPPLY 10

// Trim parts
#define PROMILLE 1000

// The max value we can get from the ADC
#define ADC_MAX 1023

uint16_t supplySenseRead() {
  uint32_t raw = getOsADC(SUPPLY_SENSE_ADC);

  // Note, the order of operations is important here as we need to keep the intermediate results
  // inside the legal range for the 32 bit int we're using.
  
  raw *= VCC;               // 1023*3300 =  3_375_900
  raw *= (R_GND+R_SUPPLY);  // *11       = 37_134_900
  raw /= R_GND;             // /1
  raw /= ADC_MAX;           // /1023     =     36_300
  raw *= SUPPLY_TRIM;       // *1000     = 36_300_000 
  raw /= PROMILLE;          // /1000     =     36_300
  
  return raw;
}
