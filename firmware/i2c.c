#include "board.h"

/*
Board needs to define:
#define TWI_SDA         GPD1
#define TWI_SCL         GPD0

Or:
#define TWI_HARDWARE 

*/

#ifdef TWI_HARDWARE

/* I2C clock in Hz */
#define SCL_CLOCK  100000L

#include <compat/twi.h>


void i2c_init(void) {
  /* initialize TWI clock: 100 kHz clock, TWPS = 0 => prescaler = 1 */
  
  TWSR = 0;                         /* no prescaler */
  TWBR = ((F_CPU/SCL_CLOCK)-16)/2;  /* must be > 10 for stable operation */
}

/*************************************************************************	
  Issues a start condition and sends address and transfer direction.
  return 0 = device accessible, 1= failed to access device
*************************************************************************/
uint8_t i2c_start(uint8_t address) {
  uint8_t   twst;

  // send START condition
  TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

  // wait until transmission completed
  while(!(TWCR & (1<<TWINT)));

  // check value of TWI Status Register. Mask prescaler bits.
  twst = TW_STATUS & 0xF8;
  if ( (twst != TW_START) && (twst != TW_REP_START)) return 1;

  // send device address
  TWDR = address;
  TWCR = (1<<TWINT) | (1<<TWEN);

  // wail until transmission completed and ACK/NACK has been received
  while(!(TWCR & (1<<TWINT)));

  // check value of TWI Status Register. Mask prescaler bits.
  twst = TW_STATUS & 0xF8;
  if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

  return 0;
}



/*************************************************************************
 Issues a start condition and sends address and transfer direction.
 If device is busy, use ack polling to wait until device is ready
 
 Input:   address and transfer direction of I2C device
*************************************************************************/
void i2c_start_wait(uint8_t address)
{
    uint8_t   twst;


    while ( 1 )
    {
	    // send START condition
	    TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
    
    	// wait until transmission completed
    	while(!(TWCR & (1<<TWINT)));
    
    	// check value of TWI Status Register. Mask prescaler bits.
    	twst = TW_STATUS & 0xF8;
    	if ( (twst != TW_START) && (twst != TW_REP_START)) continue;
    
    	// send device address
    	TWDR = address;
    	TWCR = (1<<TWINT) | (1<<TWEN);
    
    	// wail until transmission completed
    	while(!(TWCR & (1<<TWINT)));
    
    	// check value of TWI Status Register. Mask prescaler bits.
    	twst = TW_STATUS & 0xF8;
    	if ( (twst == TW_MT_SLA_NACK )||(twst ==TW_MR_DATA_NACK) ) 
    	{    	    
    	    /* device busy, send stop condition to terminate write operation */
	        TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	        
	        // wait until stop condition is executed and bus released
	        while(TWCR & (1<<TWSTO));
	        
    	    continue;
    	}
    	//if( twst != TW_MT_SLA_ACK) return 1;
    	break;
     }

}/* i2c_start_wait */


/*************************************************************************
 Issues a repeated start condition and sends address and transfer direction 

 Input:   address and transfer direction of I2C device
 
 Return:  0 device accessible
          1 failed to access device
*************************************************************************/
uint8_t i2c_rep_start(uint8_t address)
{
    return i2c_start( address );

}/* i2c_rep_start */


/*************************************************************************
 Terminates the data transfer and releases the I2C bus
*************************************************************************/
void i2c_stop(void)
{
  /* send stop condition */
  TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
  
  // wait until stop condition is executed and bus released
  while(TWCR & (1<<TWSTO)); 
}


/*************************************************************************
  Send one byte to I2C device
  
  Input:    byte to be transfered
  Return:   0 write successful 
            1 write failed
*************************************************************************/
uint8_t i2c_write( uint8_t data )
{	
  uint8_t   twst;
   
  // send data to the previously addressed device
  TWDR = data;
  TWCR = (1<<TWINT) | (1<<TWEN);

  // wait until transmission completed
  while(!(TWCR & (1<<TWINT)));

  // check value of TWI Status Register. Mask prescaler bits
  twst = TW_STATUS & 0xF8;
  if( twst != TW_MT_DATA_ACK) return 1;
  return 0;
}


/*************************************************************************
 Read one byte from the I2C device, request more data from device 
 
 Return:  byte read from I2C device
*************************************************************************/
uint8_t i2c_readAck(void) {
  TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
  while(!(TWCR & (1<<TWINT)));    

  return TWDR;

}


/*************************************************************************
 Read one byte from the I2C device, read is followed by a stop condition 
 
 Return:  byte read from I2C device
*************************************************************************/
uint8_t i2c_readNak(void) {
  TWCR = (1<<TWINT) | (1<<TWEN);
  while(!(TWCR & (1<<TWINT)));

  return TWDR;
}




#else
// Software TWI:
#include <util/delay.h>
#include <avr/io.h>

void tick(void) {
  _delay_us(4);
}

void pullSDA() {
  GPOUTPUT(TWI_SDA);  
}

void floatSDA() {
  GPINPUT(TWI_SDA);  
}

void pullSCL() {
  GPOUTPUT(TWI_SCL);  
}

void floatSCL() {
  GPINPUT(TWI_SCL);  
}


volatile uint8_t i2c_frame_error = 0;

void i2c_stop(void) {
  pullSCL();
  tick();
  pullSDA();
  tick();

  floatSCL();
  tick();
  floatSDA();
  tick();

  i2c_frame_error = 0;

  if (!GPREAD(TWI_SDA)) i2c_frame_error++;
  if (!GPREAD(TWI_SCL)) i2c_frame_error++;

  tick();
  tick();
  tick();
  tick();
}

void i2c_init(void) {
  floatSCL();
  floatSDA();

  GPCLEAR(TWI_SCL);
  GPCLEAR(TWI_SDA);
  i2c_stop_cond();
}


uint8_t i2c_start(uint8_t address) {
  pullSDA();
  tick();
  pullSCL();
  tick();

  i2c_send_byte(address);
}

uint8_t i2c_write(uint8_t data)	{
  
  for (uint8_t i = 0; i < 8; i++) {
    if ((data & 0x80) == 0x00) {
      pullSDA();
    } else {
      floatSDA();
    }

    tick();
    floatSCL();

    tick();
    pullSCL();

    data = data << 1;
  }

  floatSDA();
  tick();

  floatSCL();
  tick();

  uint8_t ack = GPREAD(TWI_SDA) ? 1 : 0;

  pullSCL();

  return ack;
}

uint8_t i2c_read(uint8_t last_byte)	{
  floatSDA();

  uint8_t res = 0;
  for (uint8_t i = 0; i < 8; i++) {
    res = res << 1;

    floatSCL();
    tick();

    if (GPREAD(TWI_SDA)) res = res | 0x01;

    pullSCL();
    tick();
  }

  if (last_byte == 0) {
    pullSDA();
  } else {
    floatSDA();
  }

  tick();
  floatSCL();

  tick();
  pullSCL();

  tick();
  floatSDA();

  return res;
}

uint8_t i2c_readAck(void) {
  i2c_read(0);
}

uint8_t i2c_readNak(void) {
  i2c_read(1);
}


#endif
