#pragma once

typedef enum {
  CS_IDLE,
  CS_POWER_UP, // Waiting for 24 V locally
  CS_POWER_0,  // Waiting for 24 V at shutter 0
  CS_POWER_1,  // Waiting for 24 V at shutter 1
  CS_POWER_2,  // Waiting for 24 V at shutter 2
  CS_ERROR
} CommandState;

void commanderInit(void);
void commanderPoll(void);
CommandState commanderState();
void commanderBeginPowerUp(void);
