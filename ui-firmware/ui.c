#include <avr/wdt.h> 
#include <stdio.h>
#include <util/delay.h>

#include "board.h"

#include "i2c.h"
#include "leds.h"
#include "uart.h"
#include "powersupply.h"
#include "co2sensor.h"
#include "bme280.h"
#include "ccs811.h"

#include "commander.h"

int main(void) {
  wdt_enable(WDTO_1S);
  ledInit();
  ledBooting();  
  uartInit();
  L("Booting UI");
  L(" i2c");
  i2c_init();
  L(" CO2");
  co2Init();
  L(" supply");
  supplyInit();

  L(" ccs811");
  ccs811_init();

  L(" bme280");
  bme280_init();
  L(" commander");
  commanderInit();
  ledBooted();
  L("\n");

  uint16_t loop = 0;
  uint8_t led = 0;

  commanderBeginPowerUp();
  uint16_t co2ppm = 0;
  
  struct ccs811_data_t ccs811Data;
  
  while (1) {
    wdt_reset();
    
    CommandState oldState = commanderState();
    commanderPoll();
    CommandState newState = commanderState();
    
    if (oldState != newState) {
      P("State change %d -> %d\n", oldState, newState);
    }
    
    if (!loop++) {
     
      uint16_t supplymV = supplyRead();

      float temperature = bme280_readTemperature(); // in °C
      float humidity = bme280_readHumidity(); // in %

      int16_t temp = 10*temperature;
      int16_t humid = humidity;
      int16_t press = bme280_readPressure()-100000;
      ccs811_write_envdata(humidity, temperature);
      ccs811_read_data(&ccs811Data);
      
      P("\t\ttemp=%d\thumid=%d\tpress=%d\tco2=%d\tsupply=%d\teCO2=%d\n", 
        temp, humid, press, co2ppm, supplymV, ccs811Data.eco2);
      

      if (!(led++ & 3)) {
        setRJ45GreenLed(1);
        co2ppm = co2ReadBlocking();
        setRJ45GreenLed(0);
      }      
    }
  }
}


