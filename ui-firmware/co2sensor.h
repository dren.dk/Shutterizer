#pragma once
#include <inttypes.h>

void co2Init(void);
void co2Disable(void);
void co2Enable(void);
uint16_t co2ReadBlocking(void);

