#pragma once

#include <inttypes.h>

void ledInit(void);

void ledSet(uint8_t mask);
void ledBooting(void);
void ledBooted(void);
void setGreenLed(uint8_t on);
void setYellowLed(uint8_t on);


#ifndef ETHERNET
void setRJ45YellowLed(uint8_t on);
#endif

void setRJ45GreenLed(uint8_t on);


