#include "leds.h"
#include "board.h"

void ledInit(void) {
  GPOUTPUT(LED_GREEN);
  GPOUTPUT(LED_YELLOW);
  
  GPOUTPUT(LED_RJ45_GREEN);
#ifndef ETHERNET
  GPOUTPUT(LED_RJ45_YELLOW);
#endif

  ledSet(0);
}

void setGreenLed(uint8_t on) {
  GPWRITE(LED_GREEN, !on);
}

void setYellowLed(uint8_t on) {
  GPWRITE(LED_YELLOW, !on);
}

#ifndef ETHERNET
void setRJ45YellowLed(uint8_t on) {
  GPWRITE(LED_RJ45_YELLOW, on);
}
#endif

void setRJ45GreenLed(uint8_t on) {
  GPWRITE(LED_RJ45_GREEN, on);
}


void ledSet(uint8_t mask) {
  setYellowLed(mask & 1);
  setGreenLed(mask & 2);
  setRJ45GreenLed(mask & 4);
#ifndef ETHERNET
  setRJ45YellowLed(mask & 8);
#endif
}

void ledBooting(void) {
  ledSet(3);
}

void ledBooted(void) {
  ledSet(2);
}

