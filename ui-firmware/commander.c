#include "commander.h"
#include "powersupply.h"

#include "uart.h"

CommandState state;

int16_t timeout;

CommandState commanderState() {
  return state;
}

void commanderInit(void) {
  state = CS_IDLE;
  setSupplyState(0);
}

void commanderBeginPowerUp(void) {
  state = CS_POWER_UP;
  setSupplyState(1);
  timeout = 3000;
  L("Powering up");
}

void commanderError() {
  state = CS_IDLE;
  setSupplyState(0); 
}

void readLine(char *buffer) {
  uint8_t i = 0;
  while (i < 0) {
    int ch = getc(stdin);
    if (ch < 0 || ch > 0xff) {
      continue;
    }

    if (ch == '\n') {
      buffer[i++] = 0;
      return;
    }
    buffer[i++] = ch;
  }
}

void commanderPoll(void) {

  if (state == CS_POWER_UP) {
    uint16_t mv = supplyRead();

    if (mv > 23000) {
      state = CS_POWER_0;
      timeout = 1000;
    } else if (timeout-- < 0) {
      commanderError();
    }
    
  } else if (state == CS_POWER_0) {
    L("!0r");
    char buffy[30];
    readLine(buffy);
    P("Got: %s\n", buffy);
    state = CS_IDLE;
  }

}

