#pragma once

#include "avr8gpio.h"

/*
| Define         | AVR | Pin | Description |
|----------------|-----|-----|-------------|
|                | PD3 | 1   | Slave TX, Master RX |
|                | PD2 | 2   | Slave RX, Master TX |
|                | PD1 | 5   | SDA         |
|                | PD0 | 6   | SCL         |
| ENCODER_BTN    | PD4 | 7   | Encoder btn |
| ENCODER_A      | PC6 | 8   |             |
| ENCODER_B      | PD7 | 9   |             |
| STOP_BUTTON    | PE6 | 10  |             |
| ETH_CS         | PB4 | 11  | aka. LED_RJ45_YELLOW |
| LCD_BACKLIGHT  | PB5 | 12  |             |
| LCD_CS         | PB6 | 13  |             |
|                | PB2 | 14  |             |
|                | PB3 | 15  |             |
|                | PB1 | 16  |             |
| RJ45_LED_1     | PF7 | 17  |             |
| SUPPLY_SENSE   | PF6 | 18  | Analog input from 1/11 voltage divider |
| MH_Z19_DISABLE | PF5 | 19  | High disables serial to CO2 sensor |
| AC_SWITCH      | PF4 | 20  | High turns on +24V supply |
*/

//#define ETHERNET

#define TWI_HARDWARE
#define TWI_SDA         GPD1
#define TWI_SCL         GPD0

#define LED_YELLOW      GPB0
#define LED_GREEN       GPD5

#define ENCODER_BTN     GPD4
#define ENCODER_A       GPC6
#define ENCODER_B       GPD7
#define STOP_BUTTON     GPE6
#define ETH_CS          GPB4
#define LED_RJ45_YELLOW ETH_CS
#define LCD_BACKLIGHT   GPB5
#define LCD_CS          GPB6
#define LED_RJ45_GREEN  GPF7

#define SUPPLY_SENSE    GPF6
#define SUPPLY_SENSE_ADC 6

#define MH_Z19_DISABLE  GPF5
#define AC_SWITCH       GPF4

