#pragma once
#include <inttypes.h>

void supplyInit(void);
uint16_t supplyRead(void);

void setSupplyState(uint8_t on);
