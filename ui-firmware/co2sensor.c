#include "co2sensor.h"

#include <string.h>
#include <util/delay.h>

#include "uart.h"
#include "leds.h"
#include "board.h"

void co2Init(void) {
  co2Disable();
}

void co2Disable(void) {
  // Disabling the serial input on the CO2 sensor by forcing it high
  uartAwaitTxIdle();
  GPOUTPUT(MH_Z19_DISABLE);
  GPSET(MH_Z19_DISABLE);
}

void co2Enable(void) {
  uartAwaitTxIdle();
  GPINPUT(MH_Z19_DISABLE);
  GPCLEAR(MH_Z19_DISABLE);
}

typedef struct {
  uint8_t sensorNumber0x01;
  uint8_t command0x86;
  uint8_t zeroes[5];
} MHZ19PayloadReadCommand;

_Static_assert(sizeof(MHZ19PayloadReadCommand) == 7, "Wrong size");

typedef struct {
  uint8_t command0x86;
  uint8_t highByte;
  uint8_t lowByte;
  uint8_t zeroes[4];
} MHZ19PayloadReadAnswer;

_Static_assert(sizeof(MHZ19PayloadReadAnswer) == 7, "Wrong size");

typedef struct {
  uint8_t starting0xff;
  union {
    uint8_t payload[7];
    MHZ19PayloadReadCommand readCommand;
    MHZ19PayloadReadAnswer readAnswer;
  } payload;
  uint8_t checksum;
} MHZ19Message;

_Static_assert(sizeof(MHZ19Message) == 9, "Wrong size");


void initCommand(MHZ19Message *msg) {
  memset(msg, 0, sizeof(MHZ19Message));
  msg->starting0xff = 0xff; 
}

uint8_t calculateCheckSum(MHZ19Message *msg) {

  uint8_t checksum = 0;
  for (uint8_t i=1; i<8; i++) {
    checksum += ((uint8_t *)msg)[i];
  }
  return (0xff - checksum) + 1;
}

void setCommandChecksum(MHZ19Message *msg) {
  msg->checksum = calculateCheckSum(msg);
}

void sendCommand(MHZ19Message *msg) {
  setCommandChecksum(msg);
  /*
  for (uint8_t i=0; i<sizeof(MHZ19Message); i++) {
    uint8_t b = ((uint8_t *)msg)[i];
    P("byte %d = %02x\n", i, b);
  } 
  */ 
  
  co2Enable();
  for (uint8_t i=0; i<sizeof(MHZ19Message); i++) {
    uartPutByte(((uint8_t *)msg)[i]);
  }  
  co2Disable();
}

uint8_t recvCommand(MHZ19Message *msg) {
  memset(msg, 0, sizeof(MHZ19Message));
  
  // I have measured a maximum of about 20 ms from the start of the command to
  // the end of the last bit of the answer, so a 30 ms timeout per bit must be plenty

  int16_t timeout = 3000;
  for (uint8_t i=0; i<sizeof(MHZ19Message); i++) {
    int byte = 0xbb;
    while (timeout > 0) {
      byte = getc(stdin);
      if (byte < 0) {
	_delay_us(10);
	timeout--;
      } else {
	timeout = 300;
	break;
      }
    }
    
    ((uint8_t *)msg)[i] = byte;   
  } 

  /*
  for (uint8_t i=0; i<sizeof(MHZ19Message); i++) {
    uint8_t b = ((uint8_t *)msg)[i];
    P("byte %d = %02x\n", i, b);
  } 
  */
  
  uint8_t correctChecksum = calculateCheckSum(msg);
  uint8_t ok = msg->checksum == correctChecksum;
  
  return ok;
}


uint16_t co2ReadBlocking(void) {
  MHZ19Message msg;

  initCommand(&msg);
  msg.payload.readCommand.sensorNumber0x01 = 0x01;
  msg.payload.readCommand.command0x86 = 0x86;
  sendCommand(&msg);

  if (recvCommand(&msg)) {
    uint16_t ppm = msg.payload.readAnswer.highByte;
    ppm <<= 8;
    ppm += msg.payload.readAnswer.lowByte;
    
    return ppm;    
  } else {
    return 0xffff;
  }
  
}

